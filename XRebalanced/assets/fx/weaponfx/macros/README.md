timediff        delay between a "trigger pull" and when the bullet actually exits the gun
maxhits         number of (ship?) penetration/bounced
ricochet        0 for penetration, 1 for bounce off
scale           for beam, damage = spec damage * (beam active time / lifetime) * scale ?
attach          1 for continuous beam to start from weapon

ammunition/reload   time to reload 1 clip
reload/rate         number of shots per second
reload/time         time between single shots

thrust/@forward     x physics/drag/@forward = maxspeed 

missile/@range      range AI will start firing
missile/@lifetime   together with engine thrust 8