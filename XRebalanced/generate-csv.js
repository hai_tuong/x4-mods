const fs = require("fs");
const glob = require("glob");
const select = require('xpath.js');
const dom = require('xmldom').DOMParser;

const dir = `E:/SteamLibrary/steamapps/common/X4 extracted/`;
const waresDoc = doc(dir + `/libraries/wares.xml`);
const skipped = [];

for (let file of glob.sync(dir + `/assets/units/size_*/macros/ship_*.xml`)) {
    let { size, id } = file.match(/size_(?<size>\w+).+?(?<id>\w+)_macro.xml/).groups;
    if (size === 'xs') continue;

    let macroDoc = doc(file);
    let hp = select(macroDoc, `//hull/@max`)[0].value;
    let mass = select(macroDoc, `//physics/@mass`)[0].value;

    let wareNode = select(waresDoc, `//ware[@id='${id}']`)[0];
    if (!wareNode) {
        skipped.push(`missing ware ${id}`);
        continue;
    }

    let min = select(wareNode, `price/@min`)[0].value;
    let average = select(wareNode, `price/@average`)[0].value;
    let max = select(wareNode, `price/@max`)[0].value;
    let hullparts = select(wareNode, `production/primary/ware[@ware='hullparts']/@amount`);
    if (!hullparts.length) {
        skipped.push(`not using hullparts ${id}`);
        continue;
    }

    let parts = hullparts[0].value;

    let storageMacro = select(macroDoc, `//connection[@ref='con_storage01']/macro/@ref`)[0].value;
    let storageFile = dir + `assets/units/size_${size}/macros/${storageMacro}.xml`;
    if (!fs.existsSync(storageFile)) {
        skipped.push(`missing storageDoc ${id} ${storageMacro}`);
        continue;
    }

    let storageDoc = doc(storageFile);
    let cargo = select(storageDoc, `/macros/macro/properties/cargo/@max`)[0].value;

    console.log(size, id, mass, hp, parts, cargo, min, average, max);
}

console.warn(skipped.join('\n'));

function doc(file) {
    let xml = fs.readFileSync(file, 'utf8');
    let doc = new dom().parseFromString(xml);
    return doc;
}